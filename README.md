# Rays of Sunshine

An example game using the ![sunshine](https://gitlab.com/pard/sunshine) 2.5D engine and the ![Pixels](https://github.com/parasyte/pixels) rendering framework.

![rays of sunshine screenshot](./scrot.png)

## Usage

`cargo build --release`

Recommend building a release version as it can be a bit laggy with the dev. build.

Eventually wasm support should work, but right now I'm waiting on upstream (winit/wgpu/pixels).
