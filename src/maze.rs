use rand::prelude::*;

use super::Map;

pub fn generate(size: usize) -> Map {
    let grid = make_grid(size);
    let carved = carve(grid, size);
    Map{width: size, height: size, map: carved}
}

fn make_grid(size: usize) -> Vec<char> {
    let mut grid = vec![' '; size * size];
    for x in 0..size {
        for y in 0..size {
            if x % 2 == 0 || y % 2 == 0 {
                grid[idx(x,y,size)] = '0';
            }
        }
    }
    grid
}

fn carve(mut grid: Vec<char>, size: usize) -> Vec<char> {
    let mut rng = rand::thread_rng();
    for x in (1..size).step_by(2) {
        for y in (1..size).step_by(2) {
            if grid[idx(x,y,size)] == ' ' {
                let north: (usize, usize) = (x+1,y);
                let south: (usize, usize) = (x-1,y);
                let east: (usize, usize) = (x,y-1);
                let west: (usize, usize) = (x,y+1);
                let mut cardinal_points: Vec<(usize, usize)> = vec![north, south, east, west];
                cardinal_points.shuffle(&mut rng);
                let mut chance = 1.0;
                for point in cardinal_points {
                    if point.0 != 0 && point.0 != size - 1 && point.1 != 0 && point.1 != size - 1 {
                        if rng.gen::<f32>() <= chance {
                            grid[idx(point.0, point.1, size)] = ' ';
                        }
                        chance -= 0.25;
                    }
                }
            }
        }
    }
    grid
}

fn idx(x: usize, y: usize, size: usize) -> usize {
    y * size + x
}
