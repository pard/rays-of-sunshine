use std::time::{Duration, Instant};

use log::error;
use pixels::{Error, Pixels, SurfaceTexture};
use winit::dpi::LogicalSize;
use winit::event::{Event, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};
use winit_input_helper::WinitInputHelper;

use sunshine::player::Player;
use sunshine::framebuffer::Framebuffer;
use sunshine::map::Map;
use sunshine::renderer::render;
use sunshine::sprite::{Sprite, Sprites};
use sunshine::texture::Texture;

mod maze;
use maze::generate;

const PI: f64 = std::f64::consts::PI;
const WIDTH: u32 = 512;
const HEIGHT: u32 = 512;
const FPS: f64 = 20.0;

async fn run(event_loop: EventLoop<()>, mut input: WinitInputHelper, window: Window) -> Result<(), Error> {
    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(WIDTH, HEIGHT, surface_texture)?
    };
    let mut game_state = GameState::new();

    let mut time = Instant::now();
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            Event::RedrawRequested(_) => {
                game_state.draw(pixels.get_frame());
                if pixels
                    .render()
                    .map_err(|e| error!("pixels.render() failed: {}", e))
                    .is_err()
                {
                    *control_flow = ControlFlow::Exit;
                    return;
                }
            },
            Event::MainEventsCleared => {
                // Update internal state and request a redraw

                // Set delta time
                let now = Instant::now();
                let dt = now.duration_since(time);
                time = now;

                game_state.update(&dt);
                window.request_redraw();
            },
            _ => ()
        };


        // Handle input events
        if input.update(event) {
            // Close events
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }

            // Resize the window
            if let Some(size) = input.window_resized() {
                pixels.resize(size.width, size.height);
            }

            // Move events
            if input.key_pressed(VirtualKeyCode::Left) || input.key_pressed(VirtualKeyCode::A) {
                game_state.direction_delta = -1.0;
            } else if input.key_pressed(VirtualKeyCode::Right) || input.key_pressed(VirtualKeyCode::D) {
                game_state.direction_delta = 1.0;
            } else if input.key_released(VirtualKeyCode::Left) || input.key_released(VirtualKeyCode::A) ||
                      input.key_released(VirtualKeyCode::Right) || input.key_released(VirtualKeyCode::D) {
                game_state.direction_delta = 0.0;
            }
            if input.key_pressed(VirtualKeyCode::Up) || input.key_pressed(VirtualKeyCode::W) {
                game_state.movement_delta = 1.0;
            } else if input.key_pressed(VirtualKeyCode::Down) || input.key_pressed(VirtualKeyCode::S) {
                game_state.movement_delta = -1.0;
            } else if input.key_released(VirtualKeyCode::Up) || input.key_released(VirtualKeyCode::W) ||
                      input.key_released(VirtualKeyCode::Down) || input.key_released(VirtualKeyCode::S) {
                game_state.movement_delta = 0.0;
            }
        }
    });
}

fn main() -> Result<(), Error> {
    env_logger::init();
    let event_loop = EventLoop::new();
    let input = WinitInputHelper::new();
    let window = {
        let size = LogicalSize::new(WIDTH as f64, HEIGHT as f64);
        WindowBuilder::new()
            .with_title("Rays of Sunshine")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop)
            .unwrap()
    };

    #[cfg(not(target_arch = "wasm32"))]
    {
        futures::executor::block_on(run(event_loop, input, window))
    }
    #[cfg(target_arch = "wasm32")]
    #[cfg(feature = "web-sys")]
    {
        std::panic::set_hook(Box::new(console_error_panic_hook::hook));
        console_log::init().expect("could not initialize logger");
        use winit::platform::web::WindowExtWebSys;
        // On wasm, append the canvas to the document body
        web_sys::window()
            .and_then(|win| win.document())
            .and_then(|doc| doc.body())
            .and_then(|body| {
                body.append_child(&web_sys::Element::from(window.canvas()))
                    .ok()
            })
            .expect("couldn't append canvas to document body");
        wasm_bindgen_futures::spawn_local(run(event_loop, input, window))
    }

}
/// Representation of the application state. In this example, a box will bounce around the screen.
struct GameState {
    framebuffer: Framebuffer,
    map: Map,
    player: Player,
    sprites: Sprites,
    wall_textures: Texture,
    mon_textures: Texture,
    direction_delta: f64,
    movement_delta: f64,
}


impl GameState {
    /// Create a new `GameState` instance that can draw a moving box.
    fn new() -> Self {
        let framebuffer: Framebuffer = Framebuffer::new(HEIGHT as usize, WIDTH as usize);

        let map = generate(15);
        for row in map.map.chunks(15) { println!("{:?}", row); };

        let player: Player = Player{
            x: 1.5,
            y: 1.5,
            direction: 1.5,
            fov: PI / 3.,
        };

        let sprites: Sprites = vec![
            //Sprite::new(13.5, 13.5, 2)
        ];

        let wall_textures: Texture = Texture::new("./walltex.png");
        let mon_textures: Texture = Texture::new("./montex.png");

        let direction_delta = 0.;
        let movement_delta = 0.;
        Self {
            framebuffer, map,
            player, sprites,
            wall_textures, mon_textures,
            direction_delta, movement_delta,
        }

    }

    /// Update the `GameState` internal state; bounce the box around the screen.
    fn update(&mut self, dt: &Duration) {
        self.player.direction += self.direction_delta * 0.05 * dt.as_secs_f64() * FPS;

        let new_x = self.player.x + self.movement_delta * self.player.direction.cos() * 0.05 * dt.as_secs_f64() * FPS;
        let new_y = self.player.y + self.movement_delta * self.player.direction.sin() * 0.05 * dt.as_secs_f64() * FPS;

        if new_x >= 0. && new_x < self.map.width as f64 && new_y >= 0. && new_y < self.map.height as f64 {
            if self.map.is_empty(new_x as usize, self.player.y as usize) { self.player.x = new_x; }
            if self.map.is_empty(self.player.x as usize, new_y as usize) { self.player.y = new_y; }
        };

        render(
            &mut self.framebuffer,
            &mut self.map,
            &mut self.player,
            &mut self.sprites,
            &self.mon_textures,
            &self.wall_textures
        );
    }

    /// Draw the `GameState` state to the frame buffer.
    ///
    /// Assumes the default texture format: [`wgpu::TextureFormat::Rgba8UnormSrgb`]
    fn draw(&self, frame: &mut [u8]) {
        for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
            let x = (i % WIDTH as usize) as i16;
            let y = (i / WIDTH as usize) as i16;
            let color = self.framebuffer.get_pixel(x as usize, y as usize).unwrap();
            let rgba = [color.r, color.g, color.b, color.a];

            pixel.copy_from_slice(&rgba);
        }
    }
}
